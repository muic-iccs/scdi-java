package io.scdi.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Scdi {

    final public static String BASE_URL = "https://scdi-api.philinelabs.net";
    final public static String API_URL = BASE_URL + "/api/v1";
    private final String userName;
    private final String apiKey;

    private Logger LOGGER = Logger.getLogger("Scdi");

    static {
        ;
        // Logger.getGlobal().setLevel(Level.ALL);
    }

    public Scdi(String userName, String apiKey) {
        this.userName = userName;
        this.apiKey = apiKey;

        setupObjectMapper();
    }

    private HttpRequestWithBody postRequest(String apiUri) {
        // "POST " + API_URL+ "/" + userName + apiUri
        return Unirest.post(API_URL+ "/" + userName + apiUri)
                .header("APIKEY", apiKey);
    }

    private GetRequest getRequest(String apiUri) {
        return Unirest.get(API_URL+ "/" + userName + apiUri)
                .header("APIKEY", apiKey);
    }

    private HttpRequestWithBody deleteRequest(String apiUri) {
        return Unirest.delete(API_URL +"/" + userName + apiUri)
                .header("APIKEY", apiKey);
    }

    private void setupObjectMapper() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper =
                    new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    public KeyValueBucket createKeyValueBucket(String bucketName) throws IOException, UnirestException {
        String apiUri = String.format("/%s?create", bucketName);
        try {
            Map<String, Object> jsonBody = new HashMap<>();
            jsonBody.put("type", "keyvalue");
            HttpResponse<JsonNode> r =
                postRequest(apiUri)
                        .header("Content-Type", "application/json")
                        .body(jsonBody)
                        .asJson();
            if (r.getStatus() != 200)
                throw new IOException(String.format("Request error (%d): %s", r.getStatus(), r.getBody()));

            return new KeyValueBucket(this, bucketName);
        } catch (IOException | UnirestException e) {
            LOGGER.warning(e.toString());
            throw e;
        }
    }

    public KeyValueBucket getKeyValueBucket(String bucketName) throws Exception {
        KeyValueBucket kvBucket = new KeyValueBucket(this, bucketName);

        try {
            kvBucket.getInfo();
            return kvBucket;
        }
        catch (UnirestException e) {
            throw new Exception(String.format("Bucket %s not found", bucketName));
        }
    }

    public String dropBucket(String bucketName) throws UnirestException {
        String apiUri = String.format("/%s?delete", bucketName);
        HttpResponse<String> r = deleteRequest(apiUri).asString();

        return r.getBody();
    }

    static abstract class BaseBucket {
        private final String bucketName;
        private final Scdi conn;

        public BaseBucket(Scdi conn, String bucketName) {
            this.bucketName = bucketName;
            this.conn = conn;
        }

        public com.fasterxml.jackson.databind.JsonNode getInfo() throws UnirestException {
            String apiUri = String.format("/%s?meta", bucketName);
            HttpResponse<com.fasterxml.jackson.databind.JsonNode> r =
                    conn.getRequest(apiUri)
                    .asObject(com.fasterxml.jackson.databind.JsonNode.class);

            return r.getBody();
        }
    }

    public static class KeyValueBucket extends BaseBucket {

        private final String apiUri;

        public KeyValueBucket(Scdi conn, String bucketName) {
            super(conn, bucketName);
            this.apiUri = String.format("/%s", super.bucketName);
        }

        public String get(String key) throws UnirestException, IOException {
            HttpResponse<String> r =
                    super.conn.getRequest(apiUri)
                            .queryString("key", key)
                            .asString();

            if (r.getStatus() != 200)
                throw new IOException(String.format("Request error (%d): %s", r.getStatus(), r.getBody()));

            return r.getBody();
        }

        public String put(String key, String value) throws UnirestException, IOException {
            HttpResponse<String> r =
                    super.conn.postRequest(apiUri)
                        .queryString("key", key)
                        .body(value)
                        .asString();

            if (r.getStatus() != 200)
                throw new IOException(String.format("Request error (%d): %s", r.getStatus(), r.getBody()));

            return r.getBody();
        }
    }


    public static void main(String[] args) {

    }
}
