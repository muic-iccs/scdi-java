import io.scdi.client.Scdi;

public class ClientDemo {

    final static private String USERNAME = null; // Put in your user name
    final static private String APIKEY = null; // and APIKEY



    void keyValueDemo() throws Exception {
        if (USERNAME == null || APIKEY == null)
            throw new Exception("Invalid USERNAME/APIKEY");

        Scdi conn = new Scdi(USERNAME, APIKEY);
        String bucketName = "javakvdemo";

        Scdi.KeyValueBucket bucket = conn.createKeyValueBucket(bucketName);
        System.out.println("info: " + bucket.getInfo());
        System.out.println("=== bucket created === ");
        bucket.put("testkey", "123");
        System.out.println("=== put === ");
        System.out.println(String.format("value: %s", bucket.get("testkey")));
        System.out.println("=== get === ");

        // try accessing it from a different means
        bucket = conn.getKeyValueBucket(bucketName);
        bucket.put("testkey", "456");
        bucket.put("moo", "1050105");
        System.out.println("=== put === ");
        System.out.println(String.format("value: %s", bucket.get("testkey")));
        System.out.println(String.format("value: %s", bucket.get("moo")));
        System.out.println("=== get === ");
        conn.dropBucket(bucketName);
        System.out.println("=== bucket dropped === ");
    }
    public static void main(String[] args) throws Exception {
        ClientDemo demo = new ClientDemo();

        demo.keyValueDemo();
    }
}
